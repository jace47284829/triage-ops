# frozen_string_literal: true

Dir["#{__dir__}/*.rb"].sort.each do |plugin|
  next if plugin.end_with?('all.rb')

  require_relative File.basename(plugin, '.rb')
end
