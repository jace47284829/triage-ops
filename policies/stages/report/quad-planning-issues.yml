.common_conditions: &common_conditions
  state: opened
  forbidden_labels:
    - bug
    - "quad-planning::complete-action"
    - "quad-planning::complete-no-action"
  milestone:
    - Any

.common_conditions_dev: &common_conditions_dev
  <<: *common_conditions
  ruby: |
    labels_expected = ['workflow::ready for development', 'quad-planning::ready']
    ENV['QUAD_PLANNING_DEV'] && /^\d+\.\d+$/ =~ milestone&.title && labels_expected.any? { |label| resource[:labels].include?(label) }

.common_conditions_secure_enablement: &common_conditions_secure_enablement
  <<: *common_conditions
  ruby: |
    labels_expected = ['workflow::ready for development', 'quad-planning::ready']
    ENV['QUAD_PLANNING_SECURE_ENABLEMENT'] && /^\d+\.\d+$/ =~ milestone&.title && labels_expected.any? { |label| resource[:labels].include?(label) }

.common_conditions_other: &common_conditions_other
  <<: *common_conditions
  ruby: |
    labels_expected = ['workflow::ready for development', 'quad-planning::ready']
    ENV['QUAD_PLANNING_OTHERS'] && /^\d+\.\d+$/ =~ milestone&.title && labels_expected.any? { |label| resource[:labels].include?(label) }

.common_summary: &common_summary
  item: |
    - [ ] #{resource[:web_url]} {{title}} {{labels}} {{milestone}}
  summary: |
    # Group: {{title}}
    {{items}}

.common_actions: &common_actions
  label:
    - "quad-planning::ready"

resource_rules:
  issues:
    summaries:
      - name: Quad Planning Issues for Dev Section
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            title: |
              #{Date.today.iso8601} - Quad Planning Issues for Dev
            summary: |
              Please quad-plan the following issues:

              {{items}}

              /label ~"section::dev" ~"triage report" ~Quality
              /assign @gl-quality/dev-qe
              /due in 8 days
        rules:
          - name: Quad Planning Issues for ~"group::source code"
            conditions:
              <<: *common_conditions_dev
              labels:
                - "group::source code"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::source code"
          - name: Quad Planning Issues for ~"group::knowledge"
            conditions:
              <<: *common_conditions_dev
              labels:
                - "group::knowledge"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::knowledge"
          - name: Quad Planning Issues for ~"group::editor"
            conditions:
              <<: *common_conditions_dev
              labels:
                - "group::editor"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::editor"
          - name: Quad Planning Issues for ~"group::access"
            conditions:
              <<: *common_conditions_dev
              labels:
                - "group::access"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::access"
          - name: Quad Planning Issues for ~"group::project management"
            conditions:
              <<: *common_conditions_dev
              labels:
                - "group::project management"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::project management"
      - name: Quad Planning Issues for Ops Section
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            title: |
              #{Date.today.iso8601} - Quad Planning Issues for Ops
            summary: |
              Please quad-plan the following issues:

              {{items}}

              /label ~"section::ops" ~"triage report" ~Quality
              /assign @gl-quality/ci-cd-ops-qe
              /due in 8 days
        rules:
          - name: Quad Planning Issues for ~"group::runner"
            conditions:
              <<: *common_conditions_other
              labels:
                - "group::runner"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::runner"
          - name: Quad Planning Issues for ~"group::package"
            conditions:
              <<: *common_conditions_other
              labels:
                - "group::package"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::package"
          - name: Quad Planning Issues for ~"group::continuous integration"
            conditions:
              <<: *common_conditions_other
              labels:
                - "group::continuous integration"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::continuous integration"
      - name: Quad Planning Issues for Enablement
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            title: |
              #{Date.today.iso8601} - Quad Planning Issues for Enablement
            summary: |
              Please quad-plan the following issues:

              {{items}}

              /label ~"section::enablement" ~"triage report" ~Quality
              /assign @gl-quality/enablement-qe
              /due in 8 days
        rules:
          - name: Quad Planning Issues for ~"group::memory"
            conditions:
              <<: *common_conditions_secure_enablement
              labels:
                - "group::memory"
              ruby: |
                labels_expected = ['Deliverable', 'quad-planning::ready']
                ENV['QUAD_PLANNING_SECURE_ENABLEMENT'] && /^\d+\.\d+$/ =~ milestone&.title && labels_expected.any? { |label| resource[:labels].include?(label) }
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::memory"
          - name: Quad Planning Issues for ~"group::geo"
            conditions:
              <<: *common_conditions_secure_enablement
              labels:
                - "group::geo"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::geo"
          - name: Quad Planning Issues for ~"group::distribution"
            conditions:
              <<: *common_conditions_secure_enablement
              labels:
                - "group::distribution"
              ruby: |
                labels_expected = ['Deliverable', 'quad-planning::ready']
                ENV['QUAD_PLANNING_SECURE_ENABLEMENT'] && /^\d+\.\d+$/ =~ milestone&.title && labels_expected.any? { |label| resource[:labels].include?(label) }
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::distribution"
          - name: Quad Planning Issues for ~"group::global search"
            conditions:
              <<: *common_conditions_secure_enablement
              labels:
                - "group::global search"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::global search"
      - name: Quad Planning Issues for Fulfillment
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            title: |
              #{Date.today.iso8601} - Quad Planning Issues for Fulfillment
            summary: |
              Please quad-plan the following issues:

              {{items}}

              /label ~"section::fulfillment" ~"triage report" ~Quality
              /assign @gl-quality/growth-qe
              /due in 8 days
        rules:
          - name: Quad Planning Issues for ~"group::purchase"
            conditions:
              <<: *common_conditions_other
              labels:
                - "group::purchase"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::purchase"
          - name: Quad Planning Issues for ~"group::provision"
            conditions:
              <<: *common_conditions_other
              labels:
                - "group::provision"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::provision"
      - name: Quad Planning Issues for Secure & Protect
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            title: |
              #{Date.today.iso8601} - Quad Planning Issues for Secure & Protect
            summary: |
              Please quad-plan the following issues:

              {{items}}

              /label ~"section::sec" ~"triage report" ~Quality
              /assign @gl-quality/secure-protect-qe
              /due in 8 days
        rules:
          - name: Quad Planning Issues for ~"group::composition analysis"
            conditions:
              <<: *common_conditions_secure_enablement
              labels:
                - "group::composition analysis"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::composition analysis"
