# frozen_string_literal: true

require_relative 'devops_labels'

module PriorityMappingHelper
  include DevopsLabels::Context

  SEVERITY_REGEX = /severity::[1-2]/
  PRIORITY_REGEX = /priority::[1-4]/

  SEVERITY_PRIORITY_MAP = {
    "severity::1" => "priority::1",
    "severity::2" => "priority::2",
  }

  def set_priority_for_bug
    priority_label = determine_priority_label

    if priority_label
      "/label #{priority_label}"
    else
      ""
    end
  end

  private

  def determine_priority_label
    if has_severity? && has_no_priority?
      "~\"#{SEVERITY_PRIORITY_MAP[severity]}\""
    end
  end

  def has_severity?
    label_names.grep(SEVERITY_REGEX).any?
  end

  def has_no_priority?
    label_names.grep(PRIORITY_REGEX).none?
  end

  def severity
    label_names.grep(SEVERITY_REGEX).first
  end
end