# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/lazy_heat_map'

RSpec.describe LazyHeatMap do
  subject { described_class.new(resources, policy_spec) }

  let(:resources) do
    [
      { labels: %w[priority::1 severity::2], web_url: 'https://gitlab.com/g/p/issues/1' },
      { labels: %w[priority::3 severity::3] },
      { labels: %w[priority::2] },
      { labels: %w[severity::3] },
      { labels: %w[] }
    ]
  end

  let(:policy_spec) do
    { conditions: { state: 'open', labels: %w[bug Category] } }
  end

  describe '#to_s' do
    it 'generates the heatmap' do
      p1s2 = '[1](https://gitlab.com/g/p/issues?state=open&label_name%5B%5D=bug&label_name%5B%5D=Category&label_name%5B%5D=priority%3A%3A1&label_name%5B%5D=severity%3A%3A2)'
      p3s3 = '[1](https://gitlab.com/g/p/issues?state=open&label_name%5B%5D=bug&label_name%5B%5D=Category&label_name%5B%5D=priority%3A%3A3&label_name%5B%5D=severity%3A%3A3)'
      p2 = '[1](https://gitlab.com/g/p/issues?state=open&label_name%5B%5D=bug&label_name%5B%5D=Category&label_name%5B%5D=priority%3A%3A2&not%5Blabel_name%5D%5B%5D=severity%3A%3A1&not%5Blabel_name%5D%5B%5D=severity%3A%3A2&not%5Blabel_name%5D%5B%5D=severity%3A%3A3&not%5Blabel_name%5D%5B%5D=severity%3A%3A4)'
      s3 = '[1](https://gitlab.com/g/p/issues?state=open&label_name%5B%5D=bug&label_name%5B%5D=Category&label_name%5B%5D=severity%3A%3A3&not%5Blabel_name%5D%5B%5D=priority%3A%3A1&not%5Blabel_name%5D%5B%5D=priority%3A%3A2&not%5Blabel_name%5D%5B%5D=priority%3A%3A3&not%5Blabel_name%5D%5B%5D=priority%3A%3A4)'
      neither = '[1](https://gitlab.com/g/p/issues?state=open&label_name%5B%5D=bug&label_name%5B%5D=Category&not%5Blabel_name%5D%5B%5D=priority%3A%3A1&not%5Blabel_name%5D%5B%5D=priority%3A%3A2&not%5Blabel_name%5D%5B%5D=priority%3A%3A3&not%5Blabel_name%5D%5B%5D=priority%3A%3A4&not%5Blabel_name%5D%5B%5D=severity%3A%3A1&not%5Blabel_name%5D%5B%5D=severity%3A%3A2&not%5Blabel_name%5D%5B%5D=severity%3A%3A3&not%5Blabel_name%5D%5B%5D=severity%3A%3A4)'

      expect(subject.to_s).to eq(<<~MARKDOWN.chomp)
        || ~"severity::1" | ~"severity::2" | ~"severity::3" | ~"severity::4" | No severity |
        |----|----|----|----|----|----|
        | ~"priority::1" | 0 | #{p1s2} | 0 | 0 | 0 |
        | ~"priority::2" | 0 | 0 | 0 | 0 | #{p2} |
        | ~"priority::3" | 0 | 0 | #{p3s3} | 0 | 0 |
        | ~"priority::4" | 0 | 0 | 0 | 0 | 0 |
        | No priority | 0 | 0 | #{s3} | 0 | #{neither} |
      MARKDOWN
    end

    context 'when all of resources have priorities and severities' do
      let(:resources) do
        [
          { labels: %w[priority::1 severity::4], web_url: 'https://gitlab.com/g/p/issues/1' },
          { labels: %w[priority::1 severity::4] },
          { labels: %w[priority::1 severity::4] },
          { labels: %w[priority::1 severity::4] },
          { labels: %w[priority::1 severity::4] }
        ]
      end

      it 'still shows No priorities/severities columns' do
        p1s4 = '[5](https://gitlab.com/g/p/issues?state=open&label_name%5B%5D=bug&label_name%5B%5D=Category&label_name%5B%5D=priority%3A%3A1&label_name%5B%5D=severity%3A%3A4)'

        expect(subject.to_s).to eq(<<~MARKDOWN.chomp)
          || ~"severity::1" | ~"severity::2" | ~"severity::3" | ~"severity::4" | No severity |
          |----|----|----|----|----|----|
          | ~"priority::1" | 0 | 0 | 0 | #{p1s4} | 0 |
          | ~"priority::2" | 0 | 0 | 0 | 0 | 0 |
          | ~"priority::3" | 0 | 0 | 0 | 0 | 0 |
          | ~"priority::4" | 0 | 0 | 0 | 0 | 0 |
          | No priority | 0 | 0 | 0 | 0 | 0 |
        MARKDOWN
      end
    end
  end
end
